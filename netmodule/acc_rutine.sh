#!/bin/bash

# Path to input device.
declare INPUT_DEVICE_PATH="/dev/ttyACM0"

# MQTT output channel
declare MQTT_OUTPUT_CHANNEL=${HOSTNAME}
# MQTT connection IP.
# declare MQTT_IP="190.2.219.40"
declare MQTT_IP="206.189.181.161"
# MQTT connection port.
declare MQTT_PORT=1883
# Initial timestamp.
declare timestamp="$( date "+%D_%T" | sed "s/\//-/g" )"

# Log variables, path to log file, max line count and reset line count.
# When the log file is over max line count, older entries are deleted so
# that the new
declare logFile="mosquitto_pub.${timestamp}.log"
declare logFileLineCountLimit=80
declare logFileLineCountReset=75
# Template for tabulating the log
declare tabulationTemplate="%-24s\t%-48s\t%s\n"

# The log table initialization.
declare infoStr=""
declare -i logFileInfoLineCount=0

# Genetare script-execution info and check and correct log limit and reset counts.
while : ; do
    infoStr="$( printf "\
%-24s	%s
%-24s	%s
%-24s	%s
%-24s	%s
%-24s	%s
%-24s	%s
%-24s	%s
%-24s	%s" \
    "INPUT_DEVICE_PATH" "${INPUT_DEVICE_PATH}" \
    "MQTT_OUTPUT_CHANNEL" "${MQTT_OUTPUT_CHANNEL}" \
    "MQTT_IP" "${MQTT_IP}" \
    "MQTT_PORT" "${MQTT_PORT}" \
    "timestamp" "${timestamp}" \
    "logFile" "${logFile}" \
    "logFileLineCountLimit" "${logFileLineCountLimit}" \
    "logFileLineCountReset" "${logFileLineCountReset}"  )"

    # Obtain quantity of lines in the information section for the log.
    logFileInfoLineCount=$( wc -l <<< "${infoStr}" )

    # Check min reset value. Should be at least logFileInfoLineCount+4(empty line, header line, at least 1 entry).
    if [[ $(( logFileInfoLineCount+3 )) -gt $(( logFileLineCountReset )) ]]; then
        logFileLineCountReset=$(( logFileInfoLineCount+3 ))
        continue
    fi

    # Check min limit value. Should be at least logFileLineCountReset+1.
    if [[ $(( logFileLineCountReset )) -gt $(( logFileLineCountLimit )) ]]; then
        logFileLineCountLimit=$(( logFileLineCountReset ))
        continue
    fi

    # Exit when everything is ok.
    break
done


# Tabulate header for logging table and initialize log file.
declare infoHeader="$( printf "${tabulationTemplate}" "Time" "message" "Log Info" )"
touch "${logFile}"
printf "${infoStr}\n\n${infoHeader}\n" >> ${logFile}
# Print final information for the script execution.
printf "${infoStr}\n\n${infoHeader}\n"

# Format the message for logging.
# use as
# echo "${outputFromMosquitto}" | tabulateLogEntry ${messageToMosquito}
# echo "${str}" | tabulateLogEntry ${LINE}
tabulateLogEntry() {
    timestamp=$( date "+%D-%T" )
    read str
    LINE=$( echo "${1}" | sed -e "s/\t/ /g" )
    printf "${tabulationTemplate}" "${timestamp}" "${LINE}" "${str}"
}

# Execute the mosquitto_pub repeatedly for the input from the device.
while : ; do
    cat ${INPUT_DEVICE_PATH} | while read LINE; do
        if [ "${LINE}" != "" ]; then
            # Publish the message.
            mosquitto_pub -h "${MQTT_IP}" -p "${MQTT_PORT}" -t "${MQTT_OUTPUT_CHANNEL}" -m "${LINE}" 2>&1 | tabulateLogEntry "${LINE}" >> "${logFile}"
            printf "\r$(tail  -1 $logFile)"

            # Limit the size of the log, delete older entries.
            declare logFileLineCount="$(wc -l < "${logFile}")"
            if [[ $(( logFileLineCount )) -gt $(( logFileLineCountLimit )) ]]; then
                cat "${logFile}" | tail -$(( logFileLineCountReset - (logFileInfoLineCount+2) )) > "${logFile}.tmp"
                printf "\r${infoStr}" > "${logFile}"
                printf "\n\n${infoHeader}\n" >> "${logFile}"
                cat "${logFile}.tmp" >> "${logFile}"
                rm "${logFile}.tmp"
            fi
        fi
    done
done
